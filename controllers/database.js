

const dateFormat = require('dateformat')

module.exports = {
  // async/await can be used only within an async function.
  getQ :  async function (arr)  {
    let  connection =  await require('./connection').getConnection() 
         var fecha = dateFormat(new Date(),'yyyy-mm-dd')
         var query = `select r.entrega_salida from registro r 
         where r.entrega_salida in (${arr})  and r.doc_transporte = '' and r.n_guia = '' and estado ='CARGADO' `

         /* var query = `select r.entrega_salida from registro r 
         where r.entrega_salida in (${arr})  and r.doc_transporte = '' and r.n_guia = '' ` */

        //console.table({query})
        var rows = await connection.query(query);
          //cierra session de la base
          //connection.end();
        return rows        
      },
    setData:async (data) =>{ 
      let  connection =  await require('./connection').getConnection()
          var fecha = { date: dateFormat(new Date(),'yyyy-mm-dd'), hour:dateFormat(new Date(),'hh:mm:ss')}
          var query = `update registro set doc_transporte = '${data.transporte}', facturado = 'FACTURADO',  n_guia = ${data.guia} ,fecha_remision='${fecha.date}', hora_remision='${fecha.hour}',estado = 'EN RUTA' where entrega_salida = ${data.salida} `

          //en ruta
          //var query = `update registro set doc_transporte = '${data.transporte}', facturado = 'FACTURADO',  n_guia = ${data.guia}  where entrega_salida = ${data.salida} `

         
          //var query = `update registro set doc_transporte = '${data.transporte}', facturado = 'FACTURADO',  n_guia = ${data.guia} ,fecha_remision='${fecha.date}', hora_remision='${fecha.hour}',estado = 'EN RUTA' where entrega_salida = ${data.salida} `
        
         // console.log({query})
          var rows = await connection.query(query) 
          
          console.log(rows)
      }  
}
  /* Proyecto echo por Armando Chavez */
  