var express = require('express');
var router = express.Router();
var imapController = require('../controllers/imapController')
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET Imap. */
router.get('/imap', function(req, res, next) {
   imapController.getImap(req)
});
module.exports = router;
